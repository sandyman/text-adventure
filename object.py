class Object:
    """
    An object
    """
    def __init__(self, name, obj):
        """
        Initialise Object instance
        :param name: name of object
        :param obj: source object
        """
        self.name = name
        self.description = obj['description']
        self.is_static = obj['static'] if 'static' in obj else False
        self.is_hidden = obj['hidden'] if 'hidden' in obj else False
        self.is_invisible = obj['invisible'] if 'invisible' in obj else False

    def __str__(self):
        """
        String representation
        :return: string representing the object
        """
        return '{} [{}]: {}'.format(
            self.name,
            'S' if self.is_static else 'D',
            self.description
        )

    def show(self):
        """
        Show the description of the object.
        :return: None
        """
        print(self.description)

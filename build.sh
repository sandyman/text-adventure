#!/bin/bash

set -ex

# docker hub username
USERNAME=sandyman

# image name
IMAGE=textadventure

# build the image
docker build -t ${USERNAME}/${IMAGE}:latest .

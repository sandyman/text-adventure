class Action:
    """
    Action class
    """
    def __init__(self, action, world=None):
        """
        Initialise Action instance
        :param action: source action
        :param world: world that the action belongs to
        """
        self.world = world
        self.description = action['description'] if 'description' in action else None
        self.commands = action['operations']

    def __str__(self):
        """
        String representation
        :return: String representation of the action
        """
        return '{}'.format(self.commands)

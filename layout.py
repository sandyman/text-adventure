world = {
    "init_text": "Welcome to Eastwest World.",
    "places": [{
        "name": "East end",
        "description": "You are standing at the East end of Eastwest World.",
        "exits": {
            "w": "West end",
            "s": "Terrace garden"
        },
        "objects": {
            "clock": {
                "description": "It's just an old clock.",
                "static": True,
                "hidden": True
            }
        },
        "start": True
    }, {
        "name": "West end",
        "description": "You are standing at the West end of Eastwest World. There's a watch here.",
        "exits": {
            "e": "East end",
            "s": "Infinity pool"
        },
        "objects": {
            "watch": {
                "description": "It's a gold watch. It may be worth something.",
                "hidden": True
            }
        }
    }, {
        "name": "Terrace garden",
        "description": "You're in a beautiful terrace garden. It looks like there's an owl in a tree.",
        "exits": {
            "n": "East end"
        },
        "objects": {
            "owl": {
                "description": "\"Who?\"",
                "static": True,
                "invisible": True
            }
        },
        "actions": {
            ("look", "owl"): {
                "description": "\"Who?\"",
                "operations": [{
                    "look": "owl"
                }]
            }
        }
    }, {
        "name": "Infinity pool",
        "description": "You're at the outdoor infinity pool. The skyline looks stunning from here.",
        "exits": {
            "n": "West end"
        },
        "objects": {
            "coin": {
                "description": "It is an old coin. It might be worth something.",
                "invisible": True
            }
        },
        "actions": {
            ("look", "pool"): {
                "description": "You have picked up a coin from the bottom of the pool.",
                "operations": [{
                    "get": "coin"
                }]
            }
        }
    }]
}

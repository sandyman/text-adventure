#!/bin/bash

# Accept argument to determine which version number is bumped. Default: "patch".
CMD='patch'
if [[ "$1" == 'major' ]] || [[ "$1" == 'minor' ]]; then
	CMD=$1
fi

# If the file VERSION doesn't yet exist, it will be created with version "0.0.1".
if [[ -f VERSION ]]; then
	BASE_STRING=`cat VERSION`
	BASE_LIST=(`echo ${BASE_STRING} | tr '.' ' '`)
	MAJOR=${BASE_LIST[0]}
	MINOR=${BASE_LIST[1]}
	PATCH=${BASE_LIST[2]}
	
	if [[ "$CMD" == 'minor' ]]; then
	    # Bump minor (reset patch)
		PATCH=0
		MINOR=$((MINOR + 1))
	elif [[ "$CMD" == 'major' ]]; then
	    # Bump major (reset patch, minor)
		PATCH=0
		MINOR=0
		MAJOR=$((MAJOR + 1))
	else
	    # Bump patch
		PATCH=$((PATCH + 1))
	fi
	NEW_VERSION="$MAJOR.$MINOR.$PATCH"
else
	NEW_VERSION="0.0.1"
fi

echo -e "New version $NEW_VERSION"
echo ${NEW_VERSION} > VERSION

class Player:
    """
    Player class.
    """
    def __init__(self, world):
        """
        Initialise Player object
        :param world: world that player is part of
        """
        self.world = world
        self.objects = {}

    def show_all_objects(self, arg):
        """
        Show description of all objects.
        :param arg: unused argument (but needed in parameter list)
        :return: None
        """
        if len(self.objects) > 0:
            print('Objects you are currently holding:')
            for _, o in self.objects.items():
                print('* {}'.format(o.name))
        else:
            print('You are currently not holding any objects.')

        return False

    def show_object(self, arg):
        """
        Show description of single object in inventory.
        :param arg: argument passed in for the observe command
        :return: False if object found, None otherwise
        """
        if len(arg) == 0 or arg not in self.objects:
            return None

        print(self.objects[arg].description)
        return False

    def get_object(self, arg, silent=False):
        """
        Get object. This means removing it from the location and adding it to the player's objects list.
        :param arg: name of the object
        :param silent: False if we want to show the messages, True otherwise
        :return: False
        """
        if len(arg) == 0:
            print('Can you be more specific?')
        else:
            msg = 'OK.'
            loc = self.world.get_current_location()
            if arg not in loc.objects:
                msg = 'No such object at this location.'
            else:
                obj = loc.objects[arg]
                if not obj.is_static:
                    self.objects[arg] = obj
                    del loc.objects[arg]
                else:
                    msg = 'Unable to pick up that object.'

            if not silent:
                print(msg)

        return False

    def drop_object(self, arg, silent=False):
        """
        Drop object. This means removing it from the player's objects list and adding it to the location.
        :param arg: name of the object
        :param silent: False if we want to show the messages, True otherwise
        :return: False
        """
        if len(arg) == 0:
            print('Can you be more specific?')
        else:
            msg = 'OK.'
            loc = self.world.get_current_location()
            if arg == 'everything':
                for name, obj in self.objects.items():
                    loc.objects[name] = obj

                self.objects = {}
            elif arg not in self.objects:
                msg = 'You are currently not holding that object.'
            else:
                obj = self.objects[arg]
                loc.objects[arg] = obj
                del self.objects[arg]

            if not silent:
                print(msg)

        return False

    def cmd_handle(self, cmnd, arg):
        """
        Handle commands if we can.
        :param cmnd: command to handle
        :param arg: optional arguments for the command
        :return: the command handler's return value, None if no handler found
        """
        commands = {
            ('look', 'observe', 'watch',): 'show_object',
            ('get',): 'get_object',
            ('put', 'drop',): 'drop_object',
            ('i', 'inventory',): 'show_all_objects',
        }

        for c in commands:
            if cmnd in c:
                return getattr(self, commands[c])(arg)

        return None

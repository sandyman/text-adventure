from action import Action
from object import Object


def if_current_location(f):
    """
    Decorator to check that this location is the current location of the player.
    :param f: the function to wrap
    :return: the wrapped function
    """
    def wrapped_f(self, *args, **kwargs):
        return f(self, *args, **kwargs) if self.world.get_current_location().name == self.name else None

    return wrapped_f


class Location:
    """
    A location
    """
    def __init__(self, loc, world):
        """
        Initialise Location instance
        :param loc: input location
        :param world: the world that this location will be a part of
        """
        self.world = world
        self.name = loc['name']
        self.description = loc['description']
        self.exits = loc['exits'] if 'exits' in loc else None
        self.visited = False
        self.actions = {}
        self.objects = {}

        # Load objects
        if 'objects' in loc:
            for n, o in loc['objects'].items():
                self.objects[n] = Object(n, o)

        # Load actions
        if 'actions' in loc:
            for c, a in loc['actions'].items():
                self.actions[c] = Action(a)

    def __str__(self):
        return self.name

    def show_objects(self, peeking=False):
        """
        Show description of all objects in this location.
        :param peeking: True if looking explicitly
        :return: None
        """
        nothing = 'There is nothing interesting to look at.'

        if len(self.objects) == 0 and peeking:
            print(nothing)
        else:
            all_invisible = True
            all_hidden = True
            for _, o in self.objects.items():
                if o.is_invisible:
                    continue

                all_invisible = False
                if not o.is_hidden:
                    print(o.description)
                    all_hidden = False

            if all_invisible:
                print(nothing)
            elif all_hidden:
                if peeking:
                    print('What do you want to look at?')
                    for name in self.objects:
                        print('* {}'.format(name))
                else:
                    print(nothing)

    @if_current_location
    def cmd_handle(self, cmnd, arg):
        """
        Handle specific commands for a location.
        :param cmnd: command to handle
        :param arg: optional argument for command
        :return: False if command handled, None otherwise
        """
        if (cmnd, arg) in self.actions:
            action = self.actions[(cmnd, arg)]
            for c in action.commands:
                k = next(iter(c))
                v = c[k]
                if k == 'move':
                    # A move must always be the only or last action
                    self.world.move_to(v)
                    return False

                if k == 'get':
                    if v not in self.objects:
                        return None

                    player = self.world.get_player()
                    player.get_object(v, True)
                    if action.description is not None:
                        print(action.description)

                    return False

                if k == 'look':
                    if v not in self.objects:
                        return None

                    print(self.objects[v].description)
                    return False

    @if_current_location
    def cmd_observe(self, cmnd, arg):
        """
        Handle specific commands for observing.
        :param cmnd: command to handle
        :param arg: optional argument for command
        :return: False if command handled, None otherwise
        """
        commands = (
            'look', 'observe', 'watch',
        )
        if cmnd not in commands or len(self.objects) == 0:
            return None

        if len(arg) == 0:
            self.show_objects(True)
        elif arg in self.objects and not self.objects[arg].is_invisible:
            self.objects[arg].show()
        else:
            print('I can\'t tell you any more about it.')

        return False

    def on_enter(self):
        """
        Hook that is called when the room is entered.
        """
        pass

    def on_exit(self):
        """
        Hook that is called just before the room is left.
        """
        pass

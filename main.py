from cmd import Cmd
from layout import world as world_layout
from world import World


class TextAdventure(Cmd):
    """
    The main class. Subclasses from Cmd.
    """
    prompt = '> '

    def __init__(self):
        """
        Initialise the TextAdventure instance.
        """
        super().__init__()

        try:
            self.world = World(world_layout)
        except RuntimeError as e:
            print('Oops. Something went wrong:\n    {}'.format(e))
            exit(1)

    def do_help(self, arg):
        """
        We override the built-in help command.
        :param arg: argument
        :return: None
        """
        pass

    def precmd(self, line):
        """
        Make sure we only deal with lowercase characters
        :param line: the line that was input by the user
        :return: lowercased line
        """
        return line.lower()

    def default(self, line):
        """
        We don't have any predefined commands.
        :param line: the line that was input by the user
        :return: the result
        """
        cmnd, arg, line = self.parseline(line)

        return self.world.handle_command(cmnd, arg)

    def emptyline(self):
        """
        Empty line is just that. We override because
        otherwise the previous command would be repeated.
        :return: None
        """
        pass


if __name__ == "__main__":
    try:
        TextAdventure().cmdloop()
    except KeyboardInterrupt:
        print('\nOk, bye.')

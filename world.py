from location import Location
from player import Player


class World:
    """
    The World class
    """
    def __init__(self, layout):
        """
        Initialise instance
        """
        self.current_location = None
        self.init_text = layout['init_text']
        self.locations = {}
        self.commands = []
        self.player = Player(self)

        # Add player's command handler
        self.add_command_handler(self.player)

        # Get all the places that can be visited
        self.load_locations(layout['places'])

        # Add command handlers
        self.add_command_handlers()

        # The world needs a starting location for the player
        if self.current_location is None:
            raise RuntimeError("No starting location found.")

        # And the world needs to be consistent too
        if not self.is_consistent_world():
            raise RuntimeError("The world does not seem to be consistent.")

        print('{}\n'.format(self.init_text))
        self.current_location.on_enter()
        self.show()

    def load_locations(self, locations):
        """
        Load locations in memory.
        :param locations: locations to load
        :return: None
        """
        for loc in locations:
            name = loc['name']

            # Check for duplicate name
            if name in self.locations:
                raise RuntimeError("Duplicate location found: {}".format(name))

            # Create Location object and add to the location dictionary
            location = Location(loc, self)
            self.locations[name] = location

            # Add handler for location specific commands
            self.add_command_handler(location)

            # Starting location
            if 'start' in loc:
                if self.current_location is not None:
                    raise RuntimeError("Multiple starting locations found.")

                # Set current location to the starting location
                self.current_location = location

    def add_command_handlers(self):
        """
        Add command handlers, which are indicated by the prefix cmd_
        :return: None
        """
        for member in dir(self):
            if member[:4] == 'cmd_':
                self.commands.append(getattr(self, member))

    def add_command_handler(self, instance):
        """
        Add command handler for specified class by its name.
        :param instance: the instance to use
        :return: None
        """
        for member in dir(instance):
            if member[:4] == 'cmd_':
                self.commands.append(getattr(instance, member))

    def is_consistent_world(self):
        """
        Check whether the world passed in is actually consistent. We may check for things like:
        :return: True if world is consistent, False otherwise
        """
        # Check that an exit refers to an existing room
        for _, location in self.locations.items():
            if location.exits is not None:
                for _, d in location.exits.items():
                    if d not in self.locations:
                        return False

        return True

    def show(self):
        """
        Show description of the current location.
        :return: None
        """
        loc = self.current_location
        first_visit = not loc.visited

        print('Location: {}'.format(loc.name))
        if first_visit:
            print(loc.description)
            loc.visited = True

        # Show the exits (or rather the directions where the exits are).
        print('Exits: {}'.format(' '.join(loc.exits).upper() if loc.exits is not None else 'None'))

    def get_current_location(self):
        """
        Get the current location.
        :return: current location
        """
        return self.current_location

    def get_player(self):
        """
        Get the player object.
        :return: player object
        """
        return self.player

    def move_to(self, destination):
        """
        Move directly to a location
        :param destination: destination to go to
        :return: None
        """
        self.current_location.on_exit()
        self.current_location = self.locations[destination]
        self.current_location.on_enter()
        self.show()

    def inventory(self, arg):
        """
        Inventory command (end of the line, so now we know there's nothing to look at)
        :param arg: unused argument (but needed in parameter list)
        :return: False
        """
        print('There is nothing interesting to look at.')
        return False

    def quit(self, arg):
        """
        Quit function
        :param arg: argument that is needed to check for '!'
        :return: True if we're quiting, False otherwise
        """
        if arg == '!':
            print('Ok, bye!')
            return True
        else:
            print('Quitting, huh!? Type quit! if you\'re sure.')
            return False

    def cmd_navigation(self, cmnd, arg):
        """
        Handle navigation command.
        :param cmnd: the command to handle
        :param arg: optional unused argument
        :return: False is command handled, None otherwise
        """
        commands = ('north', 'n', 'south', 's', 'east', 'e', 'west', 'w', 'up', 'u', 'down', 'd',)
        if cmnd not in commands:
            return None

        c = cmnd[0]
        if self.current_location.exits is not None and c in self.current_location.exits:
            destination = self.current_location.exits[c]
            self.move_to(destination)
        else:
            # It's not a valid direction to move to.
            print('You can\'t go that way.')

        return False

    def cmd_handle(self, cmnd, arg):
        """
        Handle commands if we can.
        :param cmnd: command to handle
        :param arg: argument for the command
        :return: either the return value of the command handler, or None
        """
        commands = {
            ('quit', 'q'): 'quit',
            ('look', 'observe', 'watch',): 'inventory',
        }
        for c in commands:
            if cmnd in c:
                return getattr(self, commands[c])(arg)

        return None

    def handle_command(self, cmnd, arg):
        """
        Handle command based on all handlers available.
        :param cmnd: command to handle
        :param arg: argument of command
        :return: the return value of the command handler, or None if no handler found
        """
        for fn in self.commands:
            ret = fn(cmnd, arg)
            if ret is not None:
                return ret

        # This indicates we did not actually handle the command (it is unknown).
        print("I don't understand what you mean.")

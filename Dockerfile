FROM python:3.6.5

WORKDIR /app

COPY . .

CMD ["python", "main.py"]
